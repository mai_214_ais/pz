#include <QFileDialog>
#include <iostream>
#include <string>
#include "MyList.h"
#include <sstream>

const QString DEFAULT_PATH = QString::fromStdString("/home/pndreya/new_pz/pz/");


MyList::MyList(QObject *parent) : QObject(parent){}
MyList::MyList(std::shared_ptr<Sender> sender, QObject *parent) : QObject(parent){
    this->sender = sender;
}

QVector<MyItem> MyList::items() const
{
    return mItems;
}

bool MyList::setItemAt(int index, const MyItem &item)
{
    if (index < 0 || index >= mItems.size())
        return false;

    const MyItem &oldItem = mItems.at(index);
    if (item.done == oldItem.done && item.description == oldItem.description)
        return false;

    mItems[index] = item;
    return true;
}

void MyList::appendItem()
{
    emit preItemAppended();
    QString fileName = QFileDialog::getOpenFileName(nullptr,
        tr("Open Image"), DEFAULT_PATH, tr("Image Files (*.png *.jpg *.bmp)"));
    std::cout << fileName.toStdString() << std::endl;

    MyItem item;
    item.done = false;
    item.description = fileName;
    mItems.append(item);

    emit postItemAppended();
}

void MyList::removeCompletedItems()
{
    for (int i = 0; i < mItems.size(); ) {
        if (mItems.at(i).done) {
            emit preItemRemoved(i);

            mItems.removeAt(i);

            emit postItemRemoved();
        } else {
            ++i;
        }
    }
}

void MyList::uploadTrueImage(){
    emit preUploadTrueImage();
    QString fileName = QFileDialog::getOpenFileName(nullptr,
        tr("Open Image"), DEFAULT_PATH, tr("Image Files (*.png *.jpg *.bmp)"));
    m_trueSource = fileName;

    sender->sendTrueImage(fileName.toStdString());

    emit postUploadTrueImage();
}

QString MyList::currentPath(){ 
    for (int i = 0; i < mItems.size(); ++i) {
        if (mItems.at(i).done) {
            std::cout << "currentPath(): " << mItems.at(i).description.toStdString() << std::endl;
            return mItems.at(i).description;
        }
    }
}

QString MyList::trueSource(){
    std::cout << "trueSource() " << m_trueSource.toStdString() << std::endl;
    sender->sendTrueImage(m_trueSource.toStdString());
    return m_trueSource;
}

QString MyList::recogniseTable(){
    std::cout << "Calling recognised Tables\n";
    QString ans = QString::fromStdString(sender->recognise(m_source.toStdString()));
    return ans;
}

QString MyList::mistakes(){
    std::stringstream sout;
    std::cout << "Start mistake check++++++\n";
    for (int i = 0; i < mItems.size(); ++i) {
        if (mItems.at(i).done) {
            std::string currentTestMistakes = sender->mistakes(mItems.at(i).description.toStdString());
            std::cout << "Check Test " << i << '\n' << currentTestMistakes << '\n';
            sout << currentTestMistakes << "\n=======\n";
        }
    }
    std::cout << "End mistake check++++++\n";
    std::cout << "======After got=====\n";
    std::cout << sout.str() << '\n';
    std::cout << "==================" << std::endl;
    return QString::fromStdString(sout.str());
}
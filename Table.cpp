#include "Table.h"
#include <iostream>

std::string Table::get(int row, int col){
    return table[row][col];
}

int Table::getRowNum(){
    return rows;
}

int Table::getColNum(){
    return cols;
}

void Table::print(){
    std::cout << "==================\n";
    for(int r = 0; r < getRowNum(); ++r){
        for(int c = 0; c < getColNum(); ++c){
            std::cout << get(r,c) << ' ';
        }
        std::cout << '\n';
    }
    std::cout << "==================\n";
}

Table Table::emptyTable(){
    std::vector<std::vector<std::string>> tableV(0);
    return Table(0, 0, tableV);
}

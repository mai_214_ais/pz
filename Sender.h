#ifndef SENDER_H
#define SENDER_H

#include "Table.h"
#include "httplib.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace httplib;
using namespace std;

class Sender{
protected:
    Client cli;
public:
    Sender() : cli(Client("localhost", 8080)){
    }
    virtual ~Sender(){}
    virtual void send(string sendStr) = 0;
    virtual string recognise(string imagePath) = 0;

    string mistakes(string falseImagePath){
        ifstream imageFile(falseImagePath, ios::binary);
        if (!imageFile) {
            cerr << "не найдена картинка, которую хотите отправить: " << falseImagePath << endl;
            return "не найдена картинка";
        }
        stringstream imageBuffer;
        imageBuffer << imageFile.rdbuf();

        auto res = cli.Post("/mistakes", imageBuffer.str(), "image/jpeg");
        
        return res->body;
    }

    void sendTrueImage(string imagePath){
        ifstream imageFile(imagePath, ios::binary);
        if (!imageFile) {
            cerr << "не найдена картинка, которую хотите отправить: " << imagePath << endl;
            return;
        }
        stringstream imageBuffer;
        imageBuffer << imageFile.rdbuf();

        auto res = cli.Post("/upload_true_image", imageBuffer.str(), "image/jpeg");
        if (res && res->status == 200) {
            cout << res->body << endl;
        } else {
            cerr << "Картинка не загрузилась" << endl;
        }
    }

};
#endif
#include "opencv2/opencv.hpp"
#include "FakeRecogniser.h"
#include "Table.h"

Table FakeRecogniser::recognise(cv::Mat image, std::string show_steps) {
    int rows = 5;
    int cols = 3;
    std::vector<std::vector<std::string>> table = {
        {"S", "S", "S"},
        {"S", "S", "S"},
        {"S", "S", "S"},
        {"S", "S", "S"},
        {"S", "S", "S"}
    };
    return Table(rows, cols, table);
}
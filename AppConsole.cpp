#include <string>
#include <memory>
#include <iostream>
#include "AppConsole.h"
#include "Sender.h"

using namespace std;
const int MIN_VAL = 10;
//vector < vector<int> > Check(Table t1, Table t2);
void AppConsole::run(std::shared_ptr<Sender> sender){
    cout << "run console app\n";
    int true_img_num, false_img_num;
    std::cout << "Введите номер картинки с правильными ответами: ";
    std::cin >> true_img_num;
    std::cout << "\nВведите номер картинки для проверки: ";
    std::cin >> false_img_num;
    string trueImgName = "../true/img" + to_string(true_img_num) + ".jpg";
    sender->sendTrueImage(trueImgName);
    string falseImgName = "../false/img"+ to_string(false_img_num) +".jpg";
    string res = sender->mistakes(falseImgName);
    cout << "Ошибки:\n" << res << '\n';
}

#ifndef OPENCV_RECOGNISER_H
#define OPENCV_RECOGNISER_H

#include "Recogniser.h"
#include "opencv2/opencv.hpp"


class OpencvRecogniser : public Recogniser{  
public:
    OpencvRecogniser(){}
    Table recognise(cv::Mat image, std::string show_steps) override;
};

#endif
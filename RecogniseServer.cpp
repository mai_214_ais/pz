#include <cmath>
#include <memory>
#include "opencv2/opencv.hpp"
#include "Table.h"
#include "Recogniser.h"
#include "Sender.h"
#include "Parameters.h"
#include "Print.h"
#include "RecogniseServer.h"
#include "httplib.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"
#include <string>

using namespace rapidjson;
using namespace httplib;
using namespace cv;
using namespace std;

vector < vector<int> > Check(Table t1, Table t2);
void RecogniseServer::run(std::shared_ptr<Recogniser> recogniser, std::shared_ptr<Print> print, string show_steps){
    Server svr;

    svr.Post("/upload_true_image", [](const Request& req, Response& res){
        string image_name = "../server/img/true.jpg";
        ofstream imageFile(image_name, ios::out | ios::binary);
        imageFile.write(req.body.c_str(), req.body.size());
        imageFile.close();
        res.set_content("Картинка с правильными ответами загружена на сервер", "text/plain");
    });

    svr.Post("/mistakes", [&recogniser, &print, &show_steps](const Request& req, Response& res) {
        string image_name = "../server/img/false.jpg";
        ofstream imageFile(image_name, ios::out | ios::binary);
        imageFile.write(req.body.c_str(), req.body.size());
        imageFile.close();

        Mat frame1 = imread("../server/img/true.jpg");
        if(frame1.empty()){
            res.set_content("Вы не загрузили изображение с правильными ответами", "text/plain");
            return;
        }
        Table t1 = recogniser->recognise(frame1, show_steps);
        cout << print->printTable(t1);
        Mat frame2 = imread("../server/img/false.jpg");
        if(frame2.empty()){
            res.set_content("Вы не загрузили изображение для проверки", "text/plain");
            return;
        }
        Table t2 = recogniser->recognise(frame2, show_steps);
        cout << print->printTable(t2);
        vector<vector<int>> Mist1 = Check(t1,t2);
        string mistakes = print->printMistakes(Mist1);
        res.set_content(mistakes, "text/plain");
    });

    svr.Post("/recognise", [&recogniser, &print, &show_steps](const Request& req, Response& res) {
        string image_name = "../server/img/tmp.jpg";
        ofstream imageFile(image_name, ios::out | ios::binary);
        imageFile.write(req.body.c_str(), req.body.size());
        imageFile.close();

        Mat img = imread(image_name);
        Table t1 = recogniser->recognise(img, show_steps);
        string tableString = print->printTable(t1);
        cout <<  tableString;
        res.set_content(tableString, "text/plain");
    });

    svr.listen("localhost", 8080);
}

#ifndef TODOLIST_H
#define TODOLIST_H
#include <memory>

#include <QObject>
#include <QVector>

#include "Sender.h"

struct MyItem
{
    bool done;
    QString description;
};

class MyList : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged);
    Q_PROPERTY(QString recognisedTable READ recognisedTable WRITE setRecognisedTable NOTIFY recognisedTableChanged);
    Q_PROPERTY(QString mistakesText READ mistakesText WRITE setMistakesText NOTIFY mistakesTextChanged);

public:
    explicit MyList(QObject *parent = nullptr);
    explicit MyList(std::shared_ptr<Sender> sender, QObject *parent = nullptr);

    QVector<MyItem> items() const;

    bool setItemAt(int index, const MyItem &item);

    QString recognisedTable() const { return m_recognisedTable; }
    QString mistakesText() const { return m_mistakesText; }
    QString source() const { return m_source; }
    void setSource(const QString &source) {
        if (m_source != source) {
            m_source = source;
            emit sourceChanged();
        }
    }
    void setRecognisedTable(const QString &recognisedTable){
        if (m_recognisedTable != recognisedTable){
            m_recognisedTable = recognisedTable;
            emit recognisedTableChanged();
        }
    }
    void setMistakesText(const QString &mistakesText){
        if (m_mistakesText != mistakesText){
            m_mistakesText = mistakesText;
            emit mistakesTextChanged();
        }
    }

    Q_INVOKABLE QString currentPath();
    Q_INVOKABLE QString trueSource();
    Q_INVOKABLE QString recogniseTable();
    Q_INVOKABLE QString mistakes();

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

    void sourceChanged();
    void recognisedTableChanged();
    void mistakesTextChanged();

    void preUploadTrueImage();
    void postUploadTrueImage();

public slots:
    void appendItem();
    void removeCompletedItems();

    void uploadTrueImage();

private:
    QVector<MyItem> mItems;
    QString m_source;
    QString m_trueSource;
    QString m_recognisedTable = QString::fromStdString("Empty1\nEmpty1\nEmpty1\nEmpty1\n");
    QString m_mistakesText;

    std::shared_ptr<Sender> sender;
};

#endif // TODOLIST_H
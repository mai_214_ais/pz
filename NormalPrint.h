#ifndef NORMAL_PRINT_H
#define NORMAL_PRINT_H

#include "Print.h"
#include "Table.h"

class NormalPrint : public Print{  
public:
    NormalPrint() {
        type = "normal";
    }
    string printMistakes(vector <vector<int>> Mist1) override;
    string printTable(Table& table) override;
};

#endif
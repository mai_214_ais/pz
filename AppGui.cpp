#include <string>
#include <memory>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include "AppGui.h"
#include "Sender.h"
#include "MainWindow.h"

using namespace std;
const int MIN_VAL = 10;
//vector < vector<int> > Check(Table t1, Table t2);
void AppGui::run(std::shared_ptr<Sender> sender){
    char* argv[] = { (char*)"Recogniser" };
    int argc = 1;
    QApplication app(argc, argv);
    MainWindow w(sender);
    w.show();
    app.exec();
}

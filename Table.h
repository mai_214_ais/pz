#ifndef TABLE_H
#define TABLE_H
#include <vector>
#include <string>

class Table{
private:
    std::vector<std::vector<std::string>> table;
    int rows;
    int cols;

public:    
    Table(int rows_, int cols_, std::vector<std::vector<std::string>>& table_){
        rows = rows_;
        cols = cols_;
        table = table_;
    }
    Table(){}

    std::string get(int row, int col);
    int getRowNum();
    int getColNum();
    void print();
    static Table emptyTable();
};


#endif
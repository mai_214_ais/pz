#ifndef RECOGNISE_SERVER_H
#define RECOGNISE_SERVER_H
#include "Recogniser.h"
#include "Print.h"
#include <memory>
#include "string"

class RecogniseServer{
public:
    void run(
        std::shared_ptr<Recogniser> recogniser,
        std::shared_ptr<Print> print,
        std::string show_steps
    );
};

#endif
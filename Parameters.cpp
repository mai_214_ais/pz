#include "Parameters.h"
#include <fstream>
#include <string>

std::unordered_map<std::string, std::string> loadParameters(std::string parametersFileName){
    std::ifstream file("../" + parametersFileName);
    std::string line, parameterName;
    std::string parameterValue;
    std::unordered_map<std::string, std::string> parameters;
    while(std::getline(file, line)){
        std::size_t spacePos = line.find(' ');
        parameterName = line.substr(0, spacePos);
        parameterValue = line.substr(spacePos + 1);
        parameters[parameterName] = parameterValue;
    }
    return parameters;
}
#include <string>
#include <memory>
#include <iostream>
#include "AppQml.h"
#include "Sender.h"
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QWidget>

#include "MyModel.h"
#include "MyList.h"

using namespace std;

void AppQml::run(std::shared_ptr<Sender> sender){
    char* argv[] = { (char*)"Recogniser" };
    int argc = 1;
    QApplication app(argc, argv);

    qmlRegisterType<MyModel>("My", 1, 0, "MyModel");
    qmlRegisterUncreatableType<MyList>("My", 1, 0, "MyList",
        QStringLiteral("MyList should not be created in QML"));

    MyList myList(sender);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty(QStringLiteral("myList"), &myList);
    const QUrl url(QStringLiteral("../qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

    app.exec();
}

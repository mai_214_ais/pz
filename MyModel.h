#ifndef TODOMODEL_H
#define TODOMODEL_H

#include <QAbstractListModel>

#include "MyModel.h"

class MyList;

class MyModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(MyList *list READ list WRITE setList)

public:
    explicit MyModel(QObject *parent = nullptr);

    enum {
        DoneRole = Qt::UserRole,
        DescriptionRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    MyList *list() const;
    void setList(MyList *list);

private:
    MyList *mList;
};

#endif // TODOMODEL_H
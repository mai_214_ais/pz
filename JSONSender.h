#ifndef JSON_SENDER_H
#define JSON_SENDER_H

#include "Sender.h"
#include "httplib.h"
#include <vector>
#include <string>

using namespace httplib;

class JSONSender : public Sender{  
public:
    JSONSender() : Sender(){
    }
    void send(std::string jsonString) override;
    string recognise(string imagePath) override;

};

#endif
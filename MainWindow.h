#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QString>
#include <memory>
#include "Sender.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(std::shared_ptr<Sender> sender, QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_addImgButton_clicked();

    void on_showButton_clicked();

    void on_uploadTrueImgButton_clicked();

    void on_showTrueButton_clicked();

    void on_remImgButton_clicked();

    void on_checkButton_clicked();

private:
    QStringListModel *model;
    Ui::MainWindow *ui;
    QString trueImgPath;
    std::shared_ptr<Sender> sender;
};
#endif // MAINWINDOW_H



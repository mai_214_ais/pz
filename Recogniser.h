#ifndef RECOGNISER_H
#define RECOGNISER_H

#include "Table.h"
#include "opencv2/opencv.hpp"

class Recogniser{
public:
    virtual ~Recogniser(){}
    virtual Table recognise(cv::Mat image, std::string show_steps) = 0;
};

#endif
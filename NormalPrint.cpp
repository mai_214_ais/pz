
#include "NormalPrint.h"
#include "Table.h"
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;
string NormalPrint::printMistakes(vector <vector<int>> Mist1) {
    std::stringstream sout;
    sout << "Ряд\tКолонка\n";
    for (int i=0;i<Mist1.size();i++){
        for (int j=0;j<Mist1[i].size();j++){
            sout<<Mist1[i][j]+1<<"\t";
        }
        sout<<endl;
    }
    sout<<"Общее количество ошибок: " << Mist1.size() <<endl;
    return sout.str();
}

string NormalPrint::printTable(Table& table){
    std::stringstream sout;
    sout << "=== " << table.getRowNum() << ' ' << table.getColNum() << " ===\n";
    for(int r = 0; r < table.getRowNum(); ++r){
        sout << (r + 1) << " | ";
        for(int c = 0; c < table.getColNum(); ++c){
            sout << table.get(r,c) << ' ';
        }
        sout << '\n';
    }
    sout << "===\n";
    return sout.str();
}
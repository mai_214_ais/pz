#include <vector>
#include <string>
#include <memory>
#include <iostream>
#include <unordered_map>
#include "Recogniser.h"
#include "FakeRecogniser.h"
#include "OpencvRecogniser.h"
#include "Parameters.h"
#include "NormalPrint.h"
#include "JSONPrint.h"
#include "Print.h"
#include "RecogniseServer.h"

std::shared_ptr<Recogniser> recogniserFactory(string recogniser){
    if(recogniser == "fake"){
        return std::make_shared<FakeRecogniser>();
    }else if(recogniser == "opencv"){
        return std::make_shared<OpencvRecogniser>();
    }else{
        std::cerr << "Тип Recogniser указан неправильно. По-умолчанию используется Fake\n";
        return std::make_shared<FakeRecogniser>();
    }
}

std::shared_ptr<Print> printFactory(string printer){
    if(printer == "normal"){
        return std::make_shared<NormalPrint>();
    }else if(printer == "json"){
        return std::make_shared<JSONPrint>();
    }else{
        std::cerr << "Тип Print указан неправильно. По-умолчанию используется Normal\n";
        return std::make_shared<NormalPrint>();
    }
}

int main(int argc, char** argv) {
    std::string parametersFileName;
    if(argc == 1){
        parametersFileName = "ParameterValues.txt";
    }else{
        parametersFileName = argv[1];
    }
    std::unordered_map<std::string, std::string> parameters = loadParameters(parametersFileName);    
    std::shared_ptr<Recogniser> recogniser = recogniserFactory(parameters["recogniser"]);
    std::shared_ptr<Print> print = printFactory(parameters["printer"]);
    RecogniseServer srv;
    srv.run(recogniser, print, parameters["show_steps"]);
}

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "OpencvRecogniser.h"
#include "Table.h"
#include "Parameters.h"

#include <iostream>
#include <cmath>
#include <algorithm>

#define TESTS

using namespace std;
using namespace cv;


int thresh = 50, N = 11;
std::string wndname = "edges";


static double angle( Point pt1, Point pt2, Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

static void findSquares(const Mat& image, vector<vector<Point> >& squares )
{
    squares.clear();

    Mat pyr, timg, gray0(image.size(), CV_8U), gray;

    // down-scale and upscale the image to filter out the noise
    pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());
    vector<vector<Point> > contours;

    // find squares in every color plane of the image
    for( int c = 0; c < 3; c++ )
    {
        int ch[] = {c, 0};
        mixChannels(&timg, 1, &gray0, 1, ch, 1);

        // try several threshold levels
        for( int l = 0; l < N; l++)
        {
            // Canny helps to catch squares with gradient shading
            if( l == 0 )
            {
                // apply Canny. Take the upper threshold from slider
                // and set the lower to 0 (which forces edges merging)
                Canny(gray0, gray, 0, thresh, 5);
                // dilate canny output to remove potential
                // holes between edge segments
                dilate(gray, gray, Mat(), Point(-1,-1));
            }
            else
            {
                // apply threshold if l!=0:
                //     tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
                gray = gray0 >= (l+1)*255/N;
            }

            // find contours and store them all as a list
            findContours(gray, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
            //std::cout << "contour size " << contours.size() << '\n';
            vector<Point> approx;

            // test each contour
            for( size_t i = 0; i < contours.size(); i++ )
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
                approxPolyDP(contours[i], approx, arcLength(contours[i], true)*0.02, true);

                if( approx.size() == 4 &&
                    fabs(contourArea(approx)) > 200 &&
                    isContourConvex(approx) )
                {
                    double maxCosine = 0;

                    for( int j = 2; j < 5; j++ )
                    {
                        // find the maximum cosine of the angle between joint edges
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }

                    // if cosines of all angles are small
                    // (all angles are ~90 degree) then write quandrange
                    // vertices to resultant sequence
                    if( maxCosine < 0.3 )
                        squares.push_back(approx);
                }
            }
        }
    }
}

// поиск точки пересечения двух отерзков: с концами 1 и 3, с концами 2 и 4
Point getCenter(const vector<Point>& p){
    float rel0 = ((float)(p[2].y - p[0].y)) / (p[2].x - p[0].x);
    float rel1 = ((float)(p[3].y - p[1].y)) / (p[3].x - p[1].x);
    int xc = std::floor((p[0].y - p[1].y - rel0 * p[0].x + rel1 * p[1].x) / (rel1 - rel0));
    int yc = p[1].y + (xc - p[1].x) * (rel1);
    return Point(xc, yc);
}


// Лежит ли прямоугольник sr внутри прямоугольника br с погрешностью в 7 пикселей
bool insideRect(const vector<Point>& sr, const vector<Point>& br){
    Rect srr = boundingRect(sr);
    Rect brr = boundingRect(br);
    bool ly = (srr.y - brr.y) > -7;
    bool hy = (brr.y + brr.height - srr.y - srr.height) > -7;
    bool lx = (srr.x - brr.x) > -7;
    bool hx = (brr.x + brr.width - srr.x - srr.width) > -7;
    return ly && hy && lx && hx;
}

// Определяем условие сравнения клеток в таблице 
// для сортировки вырезанных клеток по строкам и столбцам
struct SquareComparator{
    bool operator()( const vector<Point>& lx, const vector<Point>& rx ) const {
        Point cl = getCenter(lx);
        Point cr = getCenter(rx);
        return ((cr.y - cl.y) > 10 || (fabs(cr.y - cl.y) < 10 && cl.x < cr.x));
    }
};

bool isCellEmpty(Mat& cellImage){
    float k = 0.9;
    int w = cellImage.size().width;
    int h = cellImage.size().height;
    Mat croppedBorders = cellImage(Rect(w * (1 - k) / 2, h * (1 - k) / 2, w * k, h * k));
    int histSize = 20;
    float range[] = { 0, 256 }; //the upper boundary is exclusive
    bool uniform = true, accumulate = false;
    const float* histRange[] = { range };
    Mat hist;
    calcHist( &croppedBorders, 1, 0, Mat(), hist, 1, &histSize, histRange, uniform, accumulate );
    int hist_w = 512, hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );
    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
    normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    float blackPixels = 0;
    for(int i = 0; i < 12; ++i){
        blackPixels += hist.at<float>(i);
    }

    return blackPixels <= 30;
}


Table OpencvRecogniser::recognise(Mat image, string show_steps){
    resize(image, image, Size(), 0.15, 0.15, INTER_LINEAR);

    vector<vector<Point>> squares;
    findSquares(image, squares);

    // findSquares() найдет также и внешние прямоугольники, а надо только клетки таблицы
    // Из всех прямоугольников оставляем только прямоугольники, обозначающие клетки
    int num_cells = squares.size();
    float imageArea = image.rows * image.cols;
    for(int fs = 0; fs < num_cells - 1; ++fs){
        while(fs < num_cells && (imageArea / fabs(contourArea(squares[fs]))) < 1.5){
            std::swap(squares[fs], squares[num_cells - 1]);
            --num_cells;
        }
        int ss = fs + 1;
        while(ss < num_cells){
            auto& sfs = squares[fs];
            double sfs_area = fabs(contourArea(sfs));
            auto& sss = squares[ss];
            double sss_area = fabs(contourArea(sss));
            
            float sRatio = sfs_area / sss_area;
            bool isSame = (0.7 < sRatio && sRatio < 1.3) &&
                            ((norm(getCenter(sfs) - getCenter(sss))) < 20);
            if(isSame){
                std::swap(squares[ss], squares[num_cells - 1]);
                --num_cells;
                continue;
            }
            if((sfs_area < sss_area && insideRect(sfs, sss)) || (sfs_area > sss_area && insideRect(sss, sfs))){
                if(sfs_area > sss_area){
                    std::swap(sfs, sss);
                    std::swap(squares[ss], squares[num_cells - 1]);
                    ss = fs + 1;
                }else{
                    std::swap(squares[ss], squares[num_cells - 1]);
                }
                --num_cells;
                continue;
            }
            ++ss;
        }
    }
    
    std::sort( squares.begin(), squares.begin() + num_cells, SquareComparator() );
    int col_num = 1;
    while(col_num < num_cells && fabs(boundingRect(squares[col_num - 1]).y - boundingRect(squares[col_num]).y) < 10){
        ++col_num;
    }
    int row_num = num_cells / col_num;
    
    if(show_steps == "yes"){
        vector<vector<Point>> true_sq_arr;
        for(int i = 0; i < num_cells; ++i){
            true_sq_arr.push_back(squares[i]);
        }
        Mat image_cp = image.clone();
        polylines(image_cp, true_sq_arr, true, Scalar(0, 255, 0), 3, LINE_AA);
        imshow(wndname, image_cp);
        waitKey(0);
        destroyAllWindows();
    }
    
    std::vector<std::vector<std::string>> table_vec(row_num, std::vector<std::string>(col_num, "E"));
    Mat grayImage;
    cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
    int curr_row = 0;
    int curr_col = 0;
    for(int sq_n = 0; sq_n < num_cells; ++sq_n){
        Mat cellImage = grayImage(boundingRect(squares[sq_n]));
        table_vec[curr_row][curr_col] = (isCellEmpty(cellImage) ? "E" : "O");
        ++curr_col;
        if(curr_col >= col_num){
            curr_col = 0;
            ++curr_row;
        }
    }
    return Table(row_num, col_num, table_vec);
}

#ifndef JSONPRINT_H
#define JSONPRINT_H

#include "Print.h"

class JSONPrint : public Print{  
public:
    JSONPrint() {
        type = "json";
    }
    string printMistakes(vector <vector<int>> Mist1) override;
    string printTable(Table& table) override;

};

#endif
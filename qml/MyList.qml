import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import My 1.0

Column{
    width : parent.width
    Row{
        width : parent.width
        Button {
            id: addListItem
            text: qsTr("+")
            onClicked : myList.appendItem()
        }
        Button {
            id: deleteListItem
            text: qsTr("-")
            onClicked : myList.removeCompletedItems()
        }
    }
    Frame {
        id : frame
        ListView {
            implicitWidth: 250
            implicitHeight: 250
            clip: true
            anchors.fill : parent

            model: MyModel {
                list: myList
            }

            delegate: RowLayout {

                CheckBox {
                    checked: model.done
                    onClicked: model.done = checked
                }
                Label {
                    text: model.description
                    Layout.fillWidth: true
                }
            }
        }
    }
}
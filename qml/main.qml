import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 1440
    height: 900
    title: qsTr("Hello, World!")

    StackView {
        id: stackView
        initialItem: mainPage
        anchors.fill: parent
    }

    Component {
        id: mainPage

        Page {
            Column{
                width : 700
                anchors.centerIn : parent
                MyList{
                }
                Button {
                    id: showFalseImage
                    text: qsTr("Показать")
                    width : parent.width
                    onClicked: {
                        myList.source = myList.currentPath()
                        myList.recognisedTable = myList.recogniseTable();
                        stackView.push(helloPage)
                    }
                }
                Button {
                    id: uploadTrueImage
                    width : parent.width
                    text: qsTr("Загрузить правильные ответы")
                    onClicked: {
                        myList.uploadTrueImage()
                    }
                }
                Button {
                    id: showTrueImage
                    width : parent.width
                    text: qsTr("Показать правильные ответы")
                    onClicked: {
                        myList.source = myList.trueSource();
                        myList.recognisedTable = myList.recogniseTable();
                        stackView.push(helloPage)
                    }
                }
                Button {
                    id: checkButton
                    width : parent.width
                    text: qsTr("Проверить выбранные тесты")
                    onClicked: {
                        myList.mistakesText = myList.mistakes();
                        stackView.push(mistakesPage)
                    }
                }
            }
        }
    }

    Component {
        id: helloPage

        Page {
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                text: qsTr("Назад")
                onClicked: {
                    stackView.pop()
                }
            }
            Column {
                anchors.centerIn: parent
                spacing: 10

                Image {
                    id: displayImage
                    source: myList.source  // Replace with your image path
                    width: 500
                    fillMode: Image.PreserveAspectFit
                }
                Text {
                    id : displayText
                    width: parent.width
                    wrapMode: Text.WordWrap//"WordWrap"   
                    elide:Text.ElideRight
                    text : myList.recognisedTable
                }
            }
        }
    }

    Component {
        id: mistakesPage

        Page {
            width : 900
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                text: qsTr("Назад")
                onClicked: {
                    stackView.pop()
                }
            }
            Column {
                anchors.centerIn: parent
                spacing: 10
                Text {
                    id : mistakesTextField
                    width: 700
                    wrapMode: Text.WordWrap//"WordWrap"   
                    elide:Text.ElideRight
                    text : myList.mistakesText
                }
            }
        }
    }

}

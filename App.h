#ifndef APP_H
#define APP_H
#include "Sender.h"
#include <memory>
#include <string>

class App{
public:
    virtual ~App(){}
    virtual void run(
        std::shared_ptr<Sender> sender
    ) = 0;
};

#endif
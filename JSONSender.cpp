// rapidjson/example/simpledom/simpledom.cpp`
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "opencv2/opencv.hpp"
#include "JSONSender.h"
#include "httplib.h"
#include <iostream>
#include <string>
#include <vector>

using namespace rapidjson;
using namespace cv;
using namespace httplib;

void JSONSender::send(std::string jsonString){
    //auto res = cli.Post("/post", s.GetString(), "application/json");
    auto res = cli.Post("/post", jsonString, "application/json");
    if (res && res->status == 200) {
        std::cout << "Response: " << res->body << std::endl;
    } else {
        std::cerr << "Request failed, error code: " << (res ? res->status : -1) << std::endl;
    }
}

string JSONSender::recognise(string imagePath){
    ifstream imageFile(imagePath, ios::binary);
    if (!imageFile) {
        cerr << "Не найдена картинка, которую хотите отправить: " << imagePath << endl;
        return "error";
    }
    stringstream imageBuffer;
    imageBuffer << imageFile.rdbuf();

    auto res = cli.Post("/recognise", imageBuffer.str(), "image/jpeg");
    if (res && res->status == 200) {
        return res->body;
    } else {
        cerr << "Картинка не загрузилась" << endl;
        return "error";
    }
}


#include <gtest/gtest.h>
#include "Recogniser.h"
#include "OpencvRecogniser.h"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include "Table.h"

#define TESTS

// Demonstrate some basic assertions.
TEST(OpencvRecogniserTest, TestImages) {
  OpencvRecogniser recogniser;
  std::vector<std::vector<int>> true_table_sizes = {{4, 4}, {7, 3}, {1, 9}, {2, 1}, {4, 4}, {4, 3}, {2, 3}};
  for(int img_num = 1; img_num <= 4; ++img_num){
    cv::Mat frame = cv::imread("../img/img" + std::to_string(img_num) + ".jpg");
    Table t = recogniser.recognise(frame);
    std::cout << t.getRowNum() << ' ' << t.getColNum() << '\n';
    EXPECT_EQ(t.getRowNum(), true_table_sizes[img_num - 1][0]);
    EXPECT_EQ(t.getColNum(), true_table_sizes[img_num - 1][1]);
  }
}
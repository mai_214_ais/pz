#ifndef APP_CONSOLE_H
#define APP_CONSOLE_H

#include "App.h"

class AppConsole : public App{  
public:
    AppConsole(){}
    void run(
        std::shared_ptr<Sender> sender
    ) override ;
};

#endif
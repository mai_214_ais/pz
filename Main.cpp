#include <vector>
#include <memory>
#include "App.h"
#include "AppConsole.h"
#include "AppGui.h"
#include "AppQml.h"
#include "Sender.h"
#include "JSONSender.h"
#include "Parameters.h"

std::shared_ptr<Sender> senderFactory(string sender){
    if(sender == "json"){
        return std::make_shared<JSONSender>();
    }else{
        return std::make_shared<JSONSender>();
    }
}

std::shared_ptr<App> appFactory(string app){
    if(app == "gui"){
        return std::make_shared<AppGui>();
    }else if(app == "qml"){
        return std::make_shared<AppQml>();
    }else{
        return std::make_shared<AppConsole>();
    }
}

int main(int argc, char** argv) {
    std::string parametersFileName;
    std::cout << "Program name: " << argv[0] << '\n';
    if(argc == 1){
        parametersFileName = "ParameterValues.txt";
    }else{
        parametersFileName = argv[1];
    }
    std::unordered_map<std::string, std::string> parameters = loadParameters(parametersFileName);    
    std::shared_ptr<Sender> sender = senderFactory(parameters["sender"]);
    std::shared_ptr<App> app = appFactory(parameters["app"]);
    app->run(sender);
}

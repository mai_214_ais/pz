#include "MainWindow.h"
#include "./ui_mainwindow.h"
#include <QMessageBox>
#include <QPixmap>
#include <iostream>
#include <QFileDialog>
#include <QStringListModel>

MainWindow::MainWindow(std::shared_ptr<Sender> sender, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    model = new QStringListModel(this);
    ui->listView->setModel(model);
    this->sender = sender;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addImgButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image"), "/home/pndreya/pz/pz/", tr("Image Files (*.png *.jpg *.bmp)"));
    QStringList stringList = model->stringList();

    // Append a new string to the list
    stringList.append(fileName);

    // Set the updated list to the model
    model->setStringList(stringList);
}


void MainWindow::on_showButton_clicked()
{
    QModelIndexList selectedIndexes = ui->listView->selectionModel()->selectedIndexes();
    if (!selectedIndexes.isEmpty()) {
        QString selectedFileName = selectedIndexes.first().data(Qt::DisplayRole).toString();
        ui->errorInfoLabel->setText(selectedFileName);
        QPixmap pic(selectedFileName);
        QSize imageSize = pic.size();
        QSize labelSize = ui->imgViewLabel->size();
        QSize scaledSize = imageSize.scaled(labelSize, Qt::KeepAspectRatio);
        // Scale the pixmap to the calculated size
        QPixmap scaledPic = pic.scaled(scaledSize, Qt::KeepAspectRatio);
        ui->imgViewLabel->setPixmap(scaledPic);

        ui->tableLabel->setText(QString::fromStdString(sender->recognise(selectedFileName.toStdString())));
    }
}


void MainWindow::on_uploadTrueImgButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image"), "/home/pndreya/pz/pz/", tr("Image Files (*.png *.jpg *.bmp)"));
    trueImgPath = fileName;
    sender->sendTrueImage(trueImgPath.toStdString());
}

void MainWindow::on_showTrueButton_clicked()
{
    ui->errorInfoLabel->setText(trueImgPath);
    QPixmap pic(trueImgPath);
    QSize imageSize = pic.size();
    QSize labelSize = ui->imgViewLabel->size();
    QSize scaledSize = imageSize.scaled(labelSize, Qt::KeepAspectRatio);

    // Scale the pixmap to the calculated size
    QPixmap scaledPic = pic.scaled(scaledSize, Qt::KeepAspectRatio);
    ui->imgViewLabel->setPixmap(scaledPic);
    
    ui->tableLabel->setText(QString::fromStdString(sender->recognise(trueImgPath.toStdString())));
}


void MainWindow::on_remImgButton_clicked()
{
    QModelIndexList selectedIndexes = ui->listView->selectionModel()->selectedIndexes();
    if(!selectedIndexes.isEmpty()){
        model->removeRows(selectedIndexes.first().row(), 1);
    }
}

void MainWindow::on_checkButton_clicked(){
    QModelIndexList selectedIndexes = ui->listView->selectionModel()->selectedIndexes();
    QMessageBox Msgbox;
    if (!selectedIndexes.isEmpty()) {
        QString selectedFileName = selectedIndexes.first().data(Qt::DisplayRole).toString();
        string mistakes = sender->mistakes(selectedFileName.toStdString());
        Msgbox.setText(QString::fromStdString(mistakes));
    }else{
        Msgbox.setText("Choose image to check");
    }
    Msgbox.exec();
}
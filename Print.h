#ifndef PRINT_H
#define PRINT_H
#include <vector>
#include <string>
#include "Table.h"

using namespace std;
class Print{
protected:
    string type;
public:
    virtual ~Print(){}
    virtual string printMistakes(vector <vector<int>> Mist1) = 0;
    virtual string printTable(Table& table) = 0;
    string getType(){
        return type;
    }
};

#endif
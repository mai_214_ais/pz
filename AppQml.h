#ifndef APP_QML_H
#define APP_QML_H

#include "App.h"

class AppQml : public App{  
public:
    AppQml(){}
    void run(
        std::shared_ptr<Sender> sender
    ) override ;
};

#endif
#include <string>
#include <cmath>
#include <memory>
#include "opencv2/opencv.hpp"
#include "Table.h"
#include "App.h"
#include "Recogniser.h"
#include "Sender.h"
#include "Parameters.h"
#include <vector>
using namespace std;
const int MIN_VAL = 10;

vector < vector<int> >Check(Table t1, Table t2){
    vector <vector<int>> Mist1;
    
    //std::cout << "Таблица размером " << t.getRowNum() << " на " << t.getColNum() << '\n';
    for(int r = 0; r < t1.getRowNum(); ++r){
        bool cor=1;
        for(int c = 0; c < t1.getColNum(); ++c){
            
            if (cor==1 && t1.get(r,c)!=t2.get(r,c)){
                vector <int> Mist2;
                Mist2.push_back(r);
                Mist2.push_back(c);
                Mist1.push_back(Mist2);
                cor=0;
                
            }
            else {
                if (cor==0) break;
            }
            
            //std::cout << t.get(r, c) << ' ';
        }
        //std::cout << '\n';
    }
    return Mist1;
    //cv::imshow("edges", frame);
    //cv::waitKey(0);
}
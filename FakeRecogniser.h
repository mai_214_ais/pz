#ifndef FAKE_RECOGNISER_H
#define FAKE_RECOGNISER_H

#include "Recogniser.h"

class FakeRecogniser : public Recogniser{  
public:
    FakeRecogniser(){}
    Table recognise(cv::Mat image, std::string show_steps) override;
};

#endif
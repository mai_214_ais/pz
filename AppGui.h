#ifndef APP_GUI_H
#define APP_GUI_H

#include "App.h"

class AppGui : public App{  
public:
    AppGui(){}
    void run(
        std::shared_ptr<Sender> sender
    ) override ;
};

#endif

#include "JSONPrint.h"
#include "Table.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <vector>
#include <iostream>
using namespace std;
using namespace rapidjson;

string JSONPrint::printMistakes(vector <vector<int>> Mist1) {
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("mistakes");
    writer.StartArray();
    for(int i = 0; i < Mist1.size(); ++i){
        writer.StartArray();
        for(int j = 0; j < Mist1.size(); ++j){
            writer.Uint(Mist1[i][j]);
        }
        writer.EndArray();
    }
    writer.EndArray();
    writer.EndObject();
    
    return s.GetString();
}

string JSONPrint::printTable(Table& table){
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("rows");
    writer.Int(table.getRowNum());
    writer.Key("cols");
    writer.Int(table.getColNum());
    writer.Key("table");
    writer.StartArray();
    for(int r = 0; r < table.getRowNum(); ++r){
        writer.StartArray();
        for(int c = 0; c < table.getColNum(); ++c){
            writer.String(table.get(r, c).c_str());
        }
        writer.EndArray();
    }
    writer.EndArray();
    writer.EndObject();
    
    return s.GetString();
}
